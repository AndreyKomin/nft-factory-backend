import * as mongoose from 'mongoose';

export const ContractSchema = new mongoose.Schema(
  {
    name: {
      type: String,
    },
    owner: {
      type: String,
    },
    address: {
      type: String,
    },
    tokenType: {
      type: String,
    },
    transaction: {
      type: String,
    },
  },
  {
    timestamps: true,
    collection: 'contracts',
  },
);
