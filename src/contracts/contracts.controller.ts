import { Body, Controller, Get, Patch, Post, Query } from '@nestjs/common';
import { ContractsService } from './contracts.service';

@Controller('contracts')
export class ContractsController {
  constructor(private readonly contractsService: ContractsService) {}

  @Get('')
  get(@Query() query) {
    return this.contractsService.get(query.accountAddress);
  }

  @Post('')
  create(@Body() body) {
    return this.contractsService.create(body);
  }

  @Patch('')
  update(@Body() body) {
    return this.contractsService.update(body);
  }
}
