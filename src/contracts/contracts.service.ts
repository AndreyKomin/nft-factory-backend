import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class ContractsService {
  constructor(
    @InjectModel('Contract') private readonly contractModel: Model<any>,
  ) {}

  get(accountAddress) {
    return this.contractModel.find({ owner: accountAddress });
  }

  create({ name, owner, address, tokenType, transaction }) {
    return this.contractModel.create({
      name,
      owner,
      address,
      tokenType,
      transaction,
    });
  }

  update({ _id, address }) {
    return this.contractModel.findByIdAndUpdate(_id, { address });
  }
}
